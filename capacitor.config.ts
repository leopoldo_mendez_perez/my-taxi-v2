import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.example.app',
  appName: 'MiTaxi',
  webDir: 'www',
  bundledWebRuntime: false,
  cordova: {
    preferences: {
      ScrollEnabled: 'false',
      BackupWebStorage: 'none',
      SplashMaintainAspectRatio: 'true',
      FadeSplashScreenDuration: '300',
      SplashShowOnlyFirstTime: 'false',
      SplashScreen: 'screen',
      SplashScreenDelay: '3000',
      GOOGLE_MAPS_ANDROID_API_KEY: 'AIzaSyBeEIqzDPhyR2ekK0-XpEWj1MmWJKw_h9k',
      GOOGLE_MAPS_IOS_API_KEY: 'AIzaSyBeEIqzDPhyR2ekK0-XpEWj1MmWJKw_h9k'
    }
  }
};

export default config;
