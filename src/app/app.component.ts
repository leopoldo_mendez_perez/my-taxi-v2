import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
       { title: 'Ingresar', url: '/login-user', icon: 'person-add' },
    { title: 'Registrarse', url: '/nuevo-login', icon: 'person-add' },
  ];
 // public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor(private storage: Storage) {
    this.api();
  }
  async api(){
    console.log("Soy Leopoldo");
    const storage = await this.storage.create();
    this.storage.set("api","http://2942-200-119-172-11.ngrok.io/");
     const api = await storage.get("api");
     console.log("codigo")
      console.log(api)
  }
}
