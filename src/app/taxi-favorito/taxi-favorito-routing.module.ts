import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TaxiFavoritoPage } from './taxi-favorito.page';

const routes: Routes = [
  {
    path: '',
    component: TaxiFavoritoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TaxiFavoritoPageRoutingModule {}
