import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TaxiFavoritoPageRoutingModule } from './taxi-favorito-routing.module';

import { TaxiFavoritoPage } from './taxi-favorito.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TaxiFavoritoPageRoutingModule
  ],
  declarations: [TaxiFavoritoPage]
})
export class TaxiFavoritoPageModule {}
