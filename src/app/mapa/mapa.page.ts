import { AfterContentInit, Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { Storage } from '@ionic/storage';
declare var google;
@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.page.html',
  styleUrls: ['./mapa.page.scss'],
})

export class MapaPage implements OnInit, AfterContentInit {
  a:any;
  map;
@ViewChild('mapElement') mapElement;
  constructor(private storage: Storage) { }

  async ngOnInit() {
    const storage = await this.storage.create();
    this.a= await storage.get("api");
    console.log("codigo")
     console.log(this.a)
  }

ngAfterContentInit(): void {
  this.map=new google.maps.Map(
    this.mapElement.nativeElement,
    {
      center: {lat: -34.397, lng:150.644},
      zoom:8
  });
}
}
