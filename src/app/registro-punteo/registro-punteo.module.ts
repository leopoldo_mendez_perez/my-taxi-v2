import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistroPunteoPageRoutingModule } from './registro-punteo-routing.module';

import { RegistroPunteoPage } from './registro-punteo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistroPunteoPageRoutingModule
  ],
  declarations: [RegistroPunteoPage]
})
export class RegistroPunteoPageModule {}
