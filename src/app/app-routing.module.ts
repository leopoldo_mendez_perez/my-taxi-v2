import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'login-user',
    loadChildren: () => import('./login-user/login-user.module').then( m => m.LoginUserPageModule)
  },

  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'register-taxista',
    loadChildren: () => import('./register-taxista/register-taxista.module').then( m => m.RegisterTaxistaPageModule)
  },
  {
    path: 'mapa',
    loadChildren: () => import('./mapa/mapa.module').then( m => m.MapaPageModule)
  },
  {
    path: 'principal',
    loadChildren: () => import('./principal/principal.module').then( m => m.PrincipalPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'nuevo-login',
    loadChildren: () => import('./nuevo-login/nuevo-login.module').then( m => m.NuevoLoginPageModule)
  },
  {
    path: 'nuevo-login',
    loadChildren: () => import('./nuevo-login/nuevo-login.module').then( m => m.NuevoLoginPageModule)
  },
  {
    path: 'registro-vehiculo',
    loadChildren: () => import('./registro-vehiculo/registro-vehiculo.module').then( m => m.RegistroVehiculoPageModule)
  },
  {
    path: 'consulta-taxi',
    loadChildren: () => import('./consulta-taxi/consulta-taxi.module').then( m => m.ConsultaTaxiPageModule)
  },
  {
    path: 'registro-punteo',
    loadChildren: () => import('./registro-punteo/registro-punteo.module').then( m => m.RegistroPunteoPageModule)
  },
  {
    path: 'taxi-favorito',
    loadChildren: () => import('./taxi-favorito/taxi-favorito.module').then( m => m.TaxiFavoritoPageModule)
  },
  {
    path: 'realizar-viaje',
    loadChildren: () => import('./realizar-viaje/realizar-viaje.module').then( m => m.RealizarViajePageModule)
  },
  {
    path: 'viajes-realizadas',
    loadChildren: () => import('./viajes-realizadas/viajes-realizadas.module').then( m => m.ViajesRealizadasPageModule)
  },


  
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
