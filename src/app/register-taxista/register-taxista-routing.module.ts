import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterTaxistaPage } from './register-taxista.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterTaxistaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterTaxistaPageRoutingModule {}
