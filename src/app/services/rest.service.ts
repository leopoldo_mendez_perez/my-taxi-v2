import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';
@Injectable({
  providedIn: 'root'
})
export class RestService {
a:any;
  constructor(public http: HttpClient,public loadingController: LoadingController,private storage: Storage,public toastController: ToastController) {
  this.a="https://e286-200-119-172-11.ngrok.io/";
  }

  loadInfo(item){
 
  return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBeEIqzDPhyR2ekK0-XpEWj1MmWJKw_h9k&address='+item);
 }
validacion(usuario,contrasenia){
  return this.http.get(this.a+"login/"+usuario+"/"+contrasenia);
 }
 extraerCodigoUsuario(usuario){
  return this.http.get(this.a+"login/consulta/"+usuario);
 }
 loginConsultar(item){
  return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBeEIqzDPhyR2ekK0-XpEWj1MmWJKw_h9k&address='+item);
 }
 RegistroUsuario(item){

  return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBeEIqzDPhyR2ekK0-XpEWj1MmWJKw_h9k&address='+item);
 }
 //Api para consultar datos para usuario cliente y taxista
 consultarDatos(nombre,telefono){

  return this.http.get(this.a+"datos/consulta/"+nombre+"/"+telefono);
 }
  //Api para consultar datos para usuario cliente y taxista
  ConsultaTaxista(login){
 
    return this.http.get(this.a+"taxista/consulta/"+login);
   }
   Consultarfechahora(){
  
    return this.http.get(this.a+"fechahora/buscar");
   }
   ConsultarIdUbicacion(cordenadax,cordenaday){
 
    return this.http.get(this.a+"ubicacion/cordenada/"+cordenadax+"/"+cordenaday);
   }
   //consultar vehiculo por placa
   ConsultarVehiculoPorPlaca(placa){
  
    return this.http.get(this.a+"vehiculo/consulta/"+placa);
   }
      //OperadorTaxi
   ConsultarOperadorTaxi(idOperador){

    return this.http.get(this.a+"taxista/consultaTaxista/"+idOperador);
   }
       //OperadorTaxi
       ConsultarDatosPersona(idDatosPersona){
    
        return this.http.get(this.a+"datos/consultaDatos/"+idDatosPersona);
       }
   //OperadorTaxi
   ConsultaLogin(idDatosPersona){

    return this.http.get(this.a+"datos/consultaDatos/"+idDatosPersona);
   }
     //OperadorTaxi
     ConsultaIdLogin(usuario,contrasenia){

      return this.http.get(this.a+"login/consultaLogin/"+usuario+"/"+contrasenia);
     }
      //Con
      ConsultarIdCliente(idLogin){

        return this.http.get(this.a+"cliente/consultaCliente/"+idLogin);
       }

}
