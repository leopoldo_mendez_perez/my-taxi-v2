import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RealizarViajePageRoutingModule } from './realizar-viaje-routing.module';

import { RealizarViajePage } from './realizar-viaje.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RealizarViajePageRoutingModule
  ],
  declarations: [RealizarViajePage]
})
export class RealizarViajePageModule {}
