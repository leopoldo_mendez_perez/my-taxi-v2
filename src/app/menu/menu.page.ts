import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  private _storage: Storage | null = null;

  constructor(private router: Router, private storage: Storage,public toastController: ToastController) { }

  ngOnInit() {
  }

location(){
  this.router.navigate(['/home'])
}
taxiFavorito(){
  this.router.navigate(['/consulta-taxi'])
}
async cerrarSesion(){
  this.router.navigate(['/principal'])
  //toast
  const salio = await this.toastController.create({
    message: 'Sesión Cerrada',
    duration: 2000
  });
  salio.present();
//almacenamiento de datos de manera temporal
const storage = await this.storage.create();
const name = await storage.set('validacion',"salio");
console.log(name);
}
calificacion(){
  this.router.navigate(['/registro-punteo'])
}
viajes_realizadas(){
  this.router.navigate(['/viajes-realizadas'])

}
}

//this._storage = storage;
//this._storage?.set('validacion','ok');