import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import {IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import  {GoogleMaps} from '@ionic-native/google-maps'
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { RestService } from './services/rest.service';
import { HttpClientModule } from '@angular/common/http';
import {Storage } from '@ionic/storage';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
//Importamos nuestro Social Sharing plugin 
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [ BrowserModule,HttpClientModule, IonicModule.forRoot(), AppRoutingModule,FormsModule     ],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    GoogleMaps,
    Geolocation,
    RestService,
    Storage,
    NativeAudio,
    SocialSharing
      ],
  bootstrap: [AppComponent],
  
})
export class AppModule {}
