import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NuevoLoginPageRoutingModule } from './nuevo-login-routing.module';

import { NuevoLoginPage } from './nuevo-login.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NuevoLoginPageRoutingModule
  ],
  declarations: [NuevoLoginPage]
})
export class NuevoLoginPageModule {}
