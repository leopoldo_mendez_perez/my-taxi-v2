import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NuevoLoginPage } from './nuevo-login.page';

const routes: Routes = [
  {
    path: '',
    component: NuevoLoginPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NuevoLoginPageRoutingModule {}
