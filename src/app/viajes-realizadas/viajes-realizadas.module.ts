import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViajesRealizadasPageRoutingModule } from './viajes-realizadas-routing.module';

import { ViajesRealizadasPage } from './viajes-realizadas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViajesRealizadasPageRoutingModule
  ],
  declarations: [ViajesRealizadasPage]
})
export class ViajesRealizadasPageModule {}
